# A WEEK AND A HALF	
## Technical document
###### Autor: Mikel Iruretagoyena Albors
###### Red Fez Studios (GNU/GPL v3) 2020

## Tabla de contenidos

0. Main
1. MainProcess
2. ExeControl
3. Core
4. Base

## Main
El main controla el modo de ejecución y llama a la generación de comandos de la terminal de debug.

	Modo de uso: ./ArreglandoElMundo [OPCIONES]
	
	Ejecuta el juego
	
	Opciones:
	  -h, --help    muestra esta ayuda
	  -d, --debug   activa el modo debug
	  -b, --box       permite visualizar las CollisionBox
	  -s, --scene   inicia en la escena indicada
	  -w, --window  inicia en modo ventana
	
	Por ejemplo:
	
	        ./ArreglandoElMundo -d --window --scene 1

## MainProcess
La clase MainProcess es la clase que da comienzo al juego. Es una clase singleton. En main.cpp, al hacer la llamada a RF_Engine::Start<>(), se le pasa como clase para el template y el propio motor se encarga de iniciarla.

Su tarea será gestionar la configuración global del juego, instanciar los controladores y managers y cambiar entre escenas.

### Atributos y miembros
#### Privados

#### Públicos

* **static MainProcess* instance:** es un puntero a la única instancia.
* **virtual void Start():** es la función a la que llamará el motor al iniciar la clase.

### Función Start
La función Start realiza las siguientes tareas:

* Da de alta los axis (con setAxis)
* Instancia "ExeControl"
* Crea la ventana
* Asigna el color de fondo a la ventana
* Crea la cámara

## ExeControl
ExeControl es la clase que controla que haya que cerrar el juego. Con los axis _exeControl_ y _exeControlAlt_ apaga el motor (y, con ello, cierra el juego).

## Core
### Scene (WIP)
La clase Scene es una factoría que se encargará (cuando funcione) de cargar y descargar las escenas.

### SceneManager (WIP)
La clase SceneManager será la encargada de almacenar todas las escenas para poder transicionar entre ellas.

### InputManager
Manager que administra los Axis.

#### Atributos y miembros

* **static void setAxis(string name, bool* positive, bool* negative):** da de alta un eje, asignando un nombre, una dirección positiva y otra negativa
* **static int getAxis(string name):** devuelve el valor total del eje indicado, devolviendo 1 si está pulsada la tecla positiva, -1 si está pulsada la negativa y 0 si no están pulsadas o si ambas están pulsadas
* **static bool False:** es una variable que contiene el valor false. Está pensada para aquellos ejes que sólo vayan a tener un valor asignado

### Camera
Clase singleton que controla la matriz de posición de la cámara.

#### Atributos y miembros

* **void Set(string _id):** indica a la cámara a qué proceso debe seguir
* **float& Speed():** velocidad a la que la cámara va a perseguir a su target

### Animation
La clase Animation encapsula la información referente a las animaciones.

#### Atributos y miembros

* **string& Package():** nombre del paquete de recursos de la animación
* **string& Basename():** nombre base de la animación
* **int& FrameAmount():** cantidad de fotogramas que tiene la animación
* **float& Speed():** velocidad a la que va a la animación (fps)
* **bool& IsLoop():** si la animación se ejecuta en bucle o no

### Animator
La clase Animator gestiona las animaciones de un mismo elemento.

#### Atributos y miembros

* **void setGraph(SDL_Surface \*\*g):** se emplea para señalar al Animator el graph en el que debe colocar los fotogramas de las animaciones
* **void Add(string aID, const char \*p, const char \*b, int f, float s = 1.0, bool l = true):** da de alta una animación asignándole un nombre clave
* **void isInList(string key):** devuelve si está dada de alta una animación
* **void tryToExecute(string state):** intenta ejecutar la animación indicada. Si no existe, mantiene la animación que ya estaba seleccionada
* **void setSpeed(float speed):** cambia la velocidad de la animación
* **float getSpeed():** obtiene la velocidad de la animación

### Sprite
La clase Sprite permite instanciar procesos con un Animator integrado y proporciona un sistema de coordenadas en referencia a la clase Camera.

#### Atributos y miembros

* **Vector2\<float> realPosition:** posición del Sprite en coordenadas relativas a la cámara
* **Vector2\<float> offset:** desplazamiento de la posición del Sprite en relación al centro del gráfico
* **virtual void OnSpriteDraw():** función a la que llama Sprite tras calcular su animación. Está pensada para que quienes hereden de ella puedan hacer uso de ella
* **void SetPosition(X x, Y y):** posiciona el sprite en las coordenadas indicadas
* **void SetPosition(Vector2\<T> pos):** posiciona el sprite en las coordenadas indicadas
* **void Mueve(X x, Y y):** mueve el sprite en la dirección indicada
* **void Mueve(Vector2\<T> pos):** mueve el sprite en la dirección indicada

### Solid
La clase Solid proporciona, heredando de Sprite, una lista de BoxColliders que permiten a los procesos tener diferentes áreas de colisión. Los colliders pueden ser de dos tipos:

* Collider: este área de colisión es sólida y si colisionan dos de este tipo, no se atravesarán
* Trigger: este área de colisión es traspasable

#### Atributos y miembros

* **vector\<BoxCollider> boxColliderList:** lista de colliders del sólido
* **Vector2\<float> force:** intensidad de las fuerzas que actúan sobre el sólido. Si se quiere mover un sólido y que las colisiones funcionen correctamente, es el atributo a usar
* **virtual void Awake():** función a la que llama Solid tras instanciarse
* **virtual void FixedUpdate():** función a la que llama Solid tras ejecutar su Update()
* **virtual void OnCollision(int _id, BoxCollider\* collider):** función a la que se llama cuando un BoxCollider de tipo collider (del sólido) colisiona con algo. Recibe el identificador del BoxCollider y el collider contra el que ha colisionado.
* **virtual void OnTrigger(int _id, BoxCollider\* collider):** función a la que se llama cuando un BoxCollider de tipo trigger (del sólido) colisiona con algo. Recibe el identificador del BoxCollider y el collider contra el que ha colisionado.
* **int addCollider(int x\_offset, int y\_offset, int width, int height, bool istrigger):** añade un BoxCollider a la lista de colliders (istrigger por defecto es false)
* **void setCollider(int \_id, int x\_offset, int y\_offset, int width, int height, bool istrigger):** modifica el collider indicado (istrigger por defecto es false)
* **void setToTrigger(int \_id, bool istrigger):** cambia el collider indicado de tipo collider a tipo trigger (y viceversa) (\_id por defecto es 1 y istrigger por defecto es true)
* **SDL_Rect normalizeBound(int \_id):** calcula las coordenadas y dimensiones del collider indicado

### Actor
La clase Actor proporciona, heredando de Solid, una máquina de estados que integra el comportamiento de los actores en este juego y el uso de las armas.

#### Estados y miembros asociados

* ST_SPAWN -> OnSpawn()
* ST_STAND -> OnStand()
* ST_ROAMING -> OnRoaming()
* ST_INSPECT -> OnInspect()
* ST_DODGE -> OnDodge()
* ST_BACKHOME -> OnBackHome()
* ST_RECOVERWEAPON -> OnRecoverWeapon()
* ST_ATTACK -> OnAttack()
* ST_DEAD -> OnDead()

#### Atributos y miembros

* **int actualState:** estado en el que se encuentra la máquina de estados
* **map\<int, string> animationRelation:** relación entre los estados y sus animaciones. Esto permite que al entrar en un estado, compruebe si ese estado tiene alguna animación asignada y, en caso afirmativo, cambia a ella
* **void setWeaponHandleOffset(float x, float y):** offset donde se coloca el arma (por defecto [0,0])
* **Weapon\* weapon:** puntero al arma activa
* **void EquipWeapon\<Weapon>():** equipa el arma indicada en el template

### Weapon
La clase Weapon proporciona, heredando de Sprite, la lógica necesaria para introducir armas en el juego, incluyendo una máquina de estados

#### Estados y miembros asociados

* ST_SPAWN -> OnSpawn()
* ST_STAND -> OnStand()
* ST_SHOOTING -> OnShoot()
* ST_RECHARGE -> OnRecharge()
* ST\_NO_AMMO -> OnNoAmmo()

#### Atributos y miembros

* **bool reorientable:** indica si el arma se reorienta en dirección al scope
* **void Equipa(int municion, int cargadores):** añade munición y cargadores al arma
