#ifndef COMANDOS_H
#define COMANDOS_H

#include <RosquilleraReforged/rf_debugconsole.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>


void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
  for(RF_Process *p : RF_TaskManager::instance->getTaskList())
  {
    if(p->type != "RF_Text")
    {
        RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
    }
  }
}

void loadPackage(int argc, const char *argv[])
{
  if(!RF_AssetManager::isLoaded(string(argv[0])))
  {
    RF_AssetManager::LoadAssetPackage("resources/"+string(argv[0]));
    RF_DebugConsoleListener::writeLine("Paquete de recursos cargado");
  }
  else
  {
    RF_DebugConsoleListener::writeLine("El paquete de recursos ya esta cargado");
  }
}

void unloadPackage(int argc, const char *argv[])
{
  if(RF_AssetManager::isLoaded(string(argv[0])))
  {
    RF_AssetManager::UnloadAssetPackage("resources/"+string(argv[0]));
    RF_DebugConsoleListener::writeLine("Paquete de recursos descargado");
  }
  else
  {
    RF_DebugConsoleListener::writeLine("El paquete de recursos no esta cargado");
  }
}

/*void GoTo(int argc, const char* argv[])
{
  int scene = atoi(argv[0]);
  MainProcess::instance->ChangeScene(scene);
  RF_DebugConsoleListener::writeLine("Escena cambiada");
}*/

/*void reload(int argc, const char* argv[])
{
	if(Level::instance != nullptr)
	{
		Level::Reload();
	}
}*/

void sendSignal(int argc, const char* argv[])
{
	string pid = string(argv[0]);
  if(!RF_Engine::existsTask(pid))
  {
    RF_DebugConsoleListener::writeLine("No existe ningun proceso con ese ID");
    return;
  }

	RF_Engine::sendSignal(pid, atoi(argv[1]));
}

void generaComandos()
{
  RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
  RF_DebugConsoleListener::addCommand("loadPackage", new RF_DebugCommand("carga un paquete de recursos", 1, &loadPackage));
  RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));
  RF_DebugConsoleListener::addCommand("unloadPackage", new RF_DebugCommand("descarga un paquete de recursos", 1, &unloadPackage));
	RF_DebugConsoleListener::addCommand("sendSignal", new RF_DebugCommand("envía una señal al proceso indicado", 2, &sendSignal));
	//RF_DebugConsoleListener::addCommand("goto", new RF_DebugCommand("cambia a la escena indicada", 1, &GoTo));
	//RF_DebugConsoleListener::addCommand("reloadLevel", new RF_DebugCommand("recarga el nivel (si esta activo)", 0, &reload));
	}
#endif //COMANDOS_H
