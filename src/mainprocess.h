#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include "core/actor.h"
#include "base/player.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_input.h>

class TestingProcess : public Actor
{
  public:
    TestingProcess():Actor("TestingProcess")
    {
      hp = 100000;
      max_hp = 100000;
      isDestructible = true;
    }
    virtual ~TestingProcess(){}

    virtual void Awake()
    {
      RF_AssetManager::LoadAssetPackage("resources/base");
      animator->Add("idle", "base", "star", 6, 12.0, true);

      animationRelation[ST_SPAWN] = "idle";
      animationRelation[ST_STAND] = "idle";
      animationRelation[ST_ROAMING] = "walk";

      addCollider(0, -100, 27, 21, false);
    }

    virtual void OnCollision(int _id, BoxCollider* collider)
    {
      collider->solid->doDamage(1);
    }
};

class MainProcess : public RF_Process
{
  public:
    static MainProcess* instance;

    MainProcess():RF_Process("MainProcess")
    {
      instance = this;
    }

    virtual ~MainProcess(){}

    virtual void Start();

    int actualScene = -1;

  private:
    string stateMachine = "";
};

#endif //MAINPROCESS_H
