#ifndef PLAYER_H
#define PLAYER_H

#include "core/actor.h"
#include "core/interfaces/alive.h"

class Player : public Actor
{
  public:
    Player():Actor("Player"){}
    virtual ~Player(){}

    virtual void Awake();

    virtual void OnStand();
    virtual void OnRoaming();

    virtual void doDamage(int damage);

  private:
    void Teclado();
    float speed = 400.0;
    float frenado = 500.0;
};

#endif //PLAYER_H
