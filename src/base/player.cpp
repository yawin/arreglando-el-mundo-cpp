#include "player.h"
#include "core/inputmanager.h"
#include "base/fireweapon.h"

#include <RosquilleraReforged/rf_assetmanager.h>

void Player::Awake()
{
  RF_AssetManager::LoadAssetPackage("resources/prota");
  animator->Add("idle", "prota", "stand_alone", 1, 12.0, true);
  animator->Add("walk", "prota", "Right", 4, 4.0, true);
  animator->Add("dead", "prota", "Dead", 4, 4.0, false);

  animationRelation[ST_SPAWN] = "idle";
  animationRelation[ST_STAND] = "idle";
  animationRelation[ST_ROAMING] = "walk";
  animationRelation[ST_DEAD] = "dead";

  max_hp = 1;
  hp = max_hp;

  setWeaponHandleOffset(0, -10);
  EquipWeapon<FireWeapon>();
}

void Player::OnStand()
{
  Teclado();
}

 void Player::OnRoaming()
 {
   if(force.x < 0)
   {
     if(flipType == FLIP_H && animator->getSpeed() < 0){ animator->setSpeed(-(animator->getSpeed())); }
     else if(flipType == FLIP_NONE && animator->getSpeed() > 0){ animator->setSpeed(-(animator->getSpeed())); }
   }
   else if(force.x > 0)
   {
     if(flipType == FLIP_NONE && animator->getSpeed() < 0){ animator->setSpeed(-(animator->getSpeed())); }
     else if(flipType == FLIP_H && animator->getSpeed() > 0){ animator->setSpeed(-(animator->getSpeed())); }
   }

   Teclado();
 }

 void Player::Teclado()
 {
   float axis_x = InputManager::getAxis("Horizontal");
   float axis_y = InputManager::getAxis("Vertical");

   force.x = axis_x * speed * RF_Engine::instance->Clock.deltaTime;
   force.y = axis_y * speed * RF_Engine::instance->Clock.deltaTime;

   if(axis_x == 0 && axis_y == 0)
   {
     actualState = ST_STAND;
   }
   else
   {
     actualState = ST_ROAMING;
   }
 }

 void Player::doDamage(int damage)
 {
   Alive::doDamage(damage);

   if(!isAlive())
   {
     actualState = ST_DEAD;
   }
 }
