#include "fireweapon.h"
#include <RosquilleraReforged/rf_assetmanager.h>

void FireWeaponScope::OnSpawn()
{
  setScopeAnimation("weapon", "scope");
}

void FireWeapon::OnSpawn()
{
  realPosition = RF_Engine::getTask<Sprite>(father)->realPosition;

  RF_AssetManager::LoadAssetPackage("resources/weapon");
  animator->Add("idle", "weapon", "subfusil", 1, 12.0, true);

  animationRelation[ST_SPAWN] = "idle";
  animationRelation[ST_STAND] = "idle";

  if("Player" == assignedTo->type)
  {
    setScope<FireWeaponScope>();
  }
}
