#ifndef FIREWEAPON_H
#define FIREWEAPON_H

#include "core/weapon.h"

class FireWeaponScope : public Scope
{
  public:
    FireWeaponScope(string name = "FireWeaponScope"):Scope(name){}
    virtual ~FireWeaponScope(){}

    virtual void OnSpawn();
};

class FireWeapon : public Weapon
{
  public:
    FireWeapon(string name = "FireWeapon"):Weapon(name){}
    virtual ~FireWeapon(){}

    virtual void OnSpawn();
};

#endif //FIREWEAPON_H
