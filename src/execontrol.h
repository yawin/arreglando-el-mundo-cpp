#ifndef EXECONTROL_H
#define EXECONTROL_H

#include "mainprocess.h"
#include "core/inputmanager.h"

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class ExeControl : public RF_Process
{
  public:
    ExeControl():RF_Process("ExeControl"){}
    virtual ~ExeControl(){}

    virtual void Update()
    {
      if(InputManager::getAxis("exeControl") != 0 || InputManager::getAxis("exeControlAlt") != 0)
      {
        if(!escPressed)
        {
          RF_Engine::Status() = false;
          escPressed = true;
        }
      }
      else
      {
        escPressed = false;
      }
    }

    bool escPressed = false;
};

#endif //EXECONTROL_H
