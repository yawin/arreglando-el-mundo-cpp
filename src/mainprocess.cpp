#include "mainprocess.h"
#include "execontrol.h"
#include "core/inputmanager.h"
#include "core/camera.h"
#include "base/player.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_input.h>

int startingScene = 0;
bool windowed = false;

MainProcess* MainProcess::instance;

void MainProcess::Start()
{
  // AXIS
    InputManager::setAxis("exeControl", &RF_Input::key[_esc], &RF_Input::jkey[_home]);
    InputManager::setAxis("exeControlAlt", &RF_Input::key[_close_window], &InputManager::False);
    InputManager::setAxis("Horizontal", &RF_Input::key[_d], &RF_Input::key[_a]);
    InputManager::setAxis("Vertical", &RF_Input::key[_s], &RF_Input::key[_w]);

  // EXECONTROL
    RF_Engine::newTask<ExeControl>(id);

  //CREAMOS LA VENTANA
    Uint32 windowFlag = (windowed) ? SDL_WINDOW_OPENGL : SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN;
    window = RF_Engine::addWindow("Arreglando el mundo", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowFlag);
    RF_Engine::MainWindow(window);

  //ASIGNAMOS EL COLOR A LA VENTANA
    RF_Engine::getWindow(window)->setBackColor(255, 255, 255);

  //CREAMOS LA CÁMARA
    RF_Engine::newTask<Camera>(id);

  //INSTANCIAMOS AL JUGADOR
    string t = RF_Engine::newTask<Player>(id);
    RF_Engine::getTask<Player>(t)->realPosition.x = (RF_Engine::getWindow(window)->width()>>1);
    RF_Engine::getTask<Player>(t)->realPosition.y = (RF_Engine::getWindow(window)->height()>>1);
    Camera::instance->Set(t);

  //INSTANCIAMOS LA ESTRELLA
    string t2 = RF_Engine::newTask<TestingProcess>(id);
    RF_Engine::getTask<TestingProcess>(t2)->realPosition.x = (RF_Engine::getWindow(window)->width()>>1) + 200;
    RF_Engine::getTask<TestingProcess>(t2)->realPosition.y = (RF_Engine::getWindow(window)->height()>>1);

}
