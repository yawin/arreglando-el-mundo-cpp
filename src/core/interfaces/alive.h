#ifndef ALIVE_H
#define ALIVE_H

#define MIN(a,b) ((a < b) ? a : b)
#define MAX(a,b) ((a > b) ? a : b)

class Alive
{
  public:
    virtual void doDamage(int damage = 1){ if(isDestructible && hp > 0){ hp = MAX(hp-damage, 0); } }
    bool isAlive(){ return (0 < hp); }

    int getHP(){ return hp; }
    void setHP(int _hp){ hp = MIN(_hp, max_hp); }
    void addHP(int _hp){ hp += MIN(_hp, max_hp); }
    void reloadHP(){ hp = max_hp; }

  protected:
    int hp = 0;
    int max_hp = 0;
    bool isDestructible = true;
};

#endif //ALIVE_H
