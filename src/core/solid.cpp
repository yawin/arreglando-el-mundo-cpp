#include "solid.h"
#include <RosquilleraReforged/rf_collision.h>
#include <RosquilleraReforged/rf_assetmanager.h>

bool viewCollisionBox = false;

string SolidCollisionCoroutine::instanceid = "";
void SolidCollisionCoroutine::Init()
{
  if(SolidCollisionCoroutine::instanceid == "")
  {
    SolidCollisionCoroutine::instanceid = RF_Engine::newTask<SolidCollisionCoroutine>();
  }
}

SolidCollisionCoroutine::~SolidCollisionCoroutine()
{
  SolidCollisionCoroutine::instanceid = "";
}

void SolidCollisionCoroutine::Update()
{
  int ic, jc;
  map<string, Solid*>::iterator i, j;

  for(i = solidList.begin(); i != solidList.end(); ++i)
  {
    for(j = i; j != solidList.end(); ++j)
    {
      if(i->first == j->first || i->second == nullptr || j->second == nullptr || i->second->signal != S_AWAKE || j->second->signal != S_AWAKE){ continue; }

      //Para cada collider suyo
      for(ic = 0; ic < i->second->boxColliderList.size(); ic++)
      {
        //Para cada collider mío
        for(jc = 0; jc < j->second->boxColliderList.size(); jc++)
        {
          //Si se chocan
          SDL_Rect a = i->second->normalizeBound(ic);
          SDL_Rect b = j->second->normalizeBound(jc);

          if(viewCollisionBox)
          {
            i->second->boxColliderList[ic].View(a);
            j->second->boxColliderList[jc].View(b);
          }

          SDL_Rect collisionRect = getIntersection(a, b);

          if(collisionRect.w == 0 && collisionRect.h == 0)
              continue;

          if(!i->second->boxColliderList[ic].isTrigger) { i->second->IsColliding(ic, &(j->second->boxColliderList[jc])); }
          else { i->second->OnTrigger(ic, &(j->second->boxColliderList[jc])); }
          if(!j->second->boxColliderList[jc].isTrigger) { j->second->IsColliding(jc, &(i->second->boxColliderList[ic])); }
          else { j->second->OnTrigger(jc, &(i->second->boxColliderList[ic])); }
        }
      }
    }
  }
}

void SolidCollisionCoroutine::RegisterSolid(Solid* solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){SolidCollisionCoroutine::Init();}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  s->solidList[solid->id] = solid;
}

void SolidCollisionCoroutine::RemoveSolid(string solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){return;}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  s->solidList[solid] = nullptr;
}

bool SolidCollisionCoroutine::IsRegistered(string solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){return false;}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  return (s->solidList[solid] != nullptr);
}

Solid::Solid(string name):Sprite(name)
{
  addCollider(0, 0, 1, 1, false);
  SolidCollisionCoroutine::Init();
}

Solid::~Solid()
{
  SolidCollisionCoroutine::RemoveSolid(id);
}

void Solid::Start()
{
  SolidCollisionCoroutine::RegisterSolid(this);
  Awake();
}

void Solid::Update()
{
  FixedUpdate();
  Move(force);
}

void Solid::Draw()
{
  Sprite::Draw();
  setCollider(0, 0, -(graph->h>>1), graph->w, graph->h, boxColliderList[0].isTrigger);
}

int Solid::addCollider(int x_offset, int y_offset, int width, int height, bool istrigger)
{
  boxColliderList.push_back(BoxCollider(this, boxColliderList.size(), {x_offset, y_offset, width, height}, istrigger));
  return boxColliderList.size() - 1;
}

void Solid::setCollider(int _id, int x_offset, int y_offset, int width, int height, bool istrigger)
{
  boxColliderList[_id].collider = {x_offset, y_offset, width, height};
  boxColliderList[_id].isTrigger = istrigger;
}

void Solid::setToTrigger(int _id, bool istrigger)
{
  boxColliderList[_id].isTrigger = istrigger;
}

SDL_Rect Solid::normalizeBound(int _id)
{
  SDL_Rect normalized;
  normalized.x = (Sint16)realPosition.x + (Sint16)offset.x + boxColliderList[_id].collider.x - (boxColliderList[_id].collider.w >> 1);
  normalized.y = (Sint16)realPosition.y + (Sint16)offset.y + boxColliderList[_id].collider.y - (boxColliderList[_id].collider.h >> 1);
  normalized.w = boxColliderList[_id].collider.w;
  normalized.h = boxColliderList[_id].collider.h;

  return normalized;
}

void Solid::IsColliding(int _id, BoxCollider* collider)
{
  //Si es el collider base y choca contra un collider sólido
    if(_id == 0 && !collider->isTrigger)
    {
      force.x = -force.x;
      force.y = -force.y;
      Move(force);
      force.x = 0;
      force.y = 0;

      collider->solid->force.x = -collider->solid->force.x;
      collider->solid->force.y = -collider->solid->force.y;
      collider->solid->Move(collider->solid->force);
      collider->solid->force.x = 0;
      collider->solid->force.y = 0;
    }

  //Llamamos al evento
    OnCollision(_id, collider);
}
