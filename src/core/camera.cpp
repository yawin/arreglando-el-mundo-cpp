#include "camera.h"
#include "sprite.h"

#include <RosquilleraReforged/rf_engine.h>

////// HASTA QUE SALGA C++20
float lerp(float a, float b, float t)
{
  return (1-t)*a + t*b;
}

Camera* Camera::instance = nullptr;

Camera::Camera():RF_Process("Camera")
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
  }

  transform.position.x = RF_Engine::MainWindow()->width()>>1;
  transform.position.y = RF_Engine::MainWindow()->height()>>1;
}

Camera::~Camera()
{
  instance = nullptr;
}

void Camera::Set(string _id)
{
  target = _id;
}

Vector2<float> Camera::RoomSize()
{
  return roomSize;
}

void Camera::Draw()
{
  if(target == ""){return;}

  Sprite* camera = RF_Engine::getTask<Sprite>(target);
  if(camera != nullptr)
  {
    float interpolation = speed * RF_Engine::instance->Clock.deltaTime;
    transform.position.x = lerp(transform.position.x, -(camera->realPosition.x + camera->offset.x - (RF_Engine::MainWindow()->width()>>1)), interpolation);
    transform.position.y = lerp(transform.position.y, -(camera->realPosition.y + camera->offset.y - (RF_Engine::MainWindow()->height()>>1)), interpolation);
  }
}
