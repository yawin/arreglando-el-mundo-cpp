#ifndef SPRITE_H
#define SPRITE_H

#include "animator.h"

#include <RosquilleraReforged/rf_process.h>

class Sprite : public RF_Process
{
  public:
    Sprite(string name = "sprite");
    virtual ~Sprite();

    virtual void Draw();
    virtual void OnSpriteDraw(){}
    virtual void LateDraw();

    template<typename X, typename Y>
    void SetPosition(X x, Y y)
    {
      realPosition.x = (float)x;
      realPosition.y = (float)y;
    }

    template<typename T>
    void SetPosition(Vector2<T> pos)
    {
      SetPosition(pos.x, pos.y);
    }

    template<typename X, typename Y>
    void Move(X x, Y y)
    {
      realPosition.x += (float)x;
      realPosition.y += (float)y;
    }

    template<typename T>
    void Move(Vector2<T> pos)
    {
      Move(pos.x, pos.y);
    }

    Vector2<float> realPosition;
    Vector2<float> offset;

  protected:
    Animator* animator;
};

#endif //SPRITE_H
