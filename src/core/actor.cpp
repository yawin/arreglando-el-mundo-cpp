#include "actor.h"

void Actor::FixedUpdate()
{
  if(weapon != nullptr)
  {
    flipType = (weapon->flipType == FLIP_NONE) ? FLIP_NONE : FLIP_H;
  }
  if(animationRelation[actualState] != "" && animator->isInList(animationRelation[actualState]))
  {
    animator->tryToExecute(animationRelation[actualState]);
  }

  switch(actualState)
  {
    case ST_SPAWN:
      OnSpawn();
      actualState = ST_STAND;
      break;
    case ST_STAND:
      OnStand();
      break;
    case ST_ROAMING:
      OnRoaming();
      break;
    case ST_INSPECT:
      OnInspect();
      break;
    case ST_DODGE:
      OnDodge();
      break;
    case ST_BACKHOME:
      OnBackHome();
      break;
    case ST_RECOVERWEAPON:
      OnRecoverWeapon();
      break;
    case ST_ATTACK:
      OnAttack();
      break;
    case ST_DEAD:
      if(weapon != nullptr)
      {
        RF_Engine::sendSignal(weapon->id, S_KILL);
        weapon = nullptr;
      }

      flipType = FLIP_NONE;
      OnDead();
      break;
  }
}

void Actor::setWeaponHandleOffset(float x, float y)
{
  weaponHandleOffset.x = x;
  weaponHandleOffset.y = y;
}
