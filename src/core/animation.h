#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>
using namespace std;

class Animation
{
  private:
    string package, basename;
    int frameAmount;
    float speed;
    bool looped;

    void save(const char *p, const char *b, int f, float s, bool l);

  public:
    Animation(const char *p, const char *b, int f, float s = 1.0, bool l = true);
    Animation(const char *b, int f, float s = 1.0, bool l = true);

    virtual ~Animation(){}

    inline string& Package(){ return package;}
    inline string& Basename(){ return basename;}
    inline int& FrameAmount(){ return frameAmount;}
    inline float& Speed(){ return speed;}
    inline bool& IsLoop(){ return looped;}
};

#endif //ANIMATION_H
