#ifndef ACTOR_H
#define ACTOR_H

class Actor;

#include "solid.h"
#include "weapon.h"

#include <string>
using namespace std;

class Actor : public Solid
{
  public:
    Actor(string name = "Actor"):Solid(name){}
    virtual ~Actor(){}

    virtual void FixedUpdate();
    Vector2<float> weaponHandleOffset;

  protected:
    enum State
    {
      ST_SPAWN = 0,
      ST_STAND = 1,
      ST_ROAMING = 2,
      ST_INSPECT = 3,
      ST_DODGE = 4,
      ST_BACKHOME = 5,
      ST_RECOVERWEAPON = 6,
      ST_ATTACK = 7,
      ST_DEAD = 8
    };

    int actualState = ST_SPAWN;
    map<int, string> animationRelation;

    virtual void OnSpawn(){}
    virtual void OnStand(){}
    virtual void OnRoaming(){}
    virtual void OnInspect(){}
    virtual void OnDodge(){}
    virtual void OnBackHome(){}
    virtual void OnRecoverWeapon(){}
    virtual void OnAttack(){}
    virtual void OnDead(){}

    void setWeaponHandleOffset(float x = 0, float y = 0);
    Weapon* weapon = nullptr;
    template<typename W>
    void EquipWeapon()
    {
      weapon = RF_Engine::getTask<Weapon>(RF_Engine::newTask<W>(id));
    }
};

#endif //ACTOR_H
