#ifndef SCENE_H
#define SCENE_H

#include <RosquilleraReforged/rf_process.h>

#include <vector>
using namespace std;

class Element
{
  public:
    template <typename T>
    Element(string name = "")
    {
      //tipo = T;
      nombre = name;
    }

    virtual ~Element(){}

    string nombre;
    //typename tipo;
};

class Scene
{
  public:
    Scene(){}
    virtual ~Scene();

    template <typename T>
    void AddElement(string name)
    {
      static_assert(std::is_base_of<RF_Process, T>::value, "T must derive from RF_Process");
      //content.push_back(new Element<T>(name));
    }

  private:
    vector<Element*> content;
};

#endif //SCENE_H
