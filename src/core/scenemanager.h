#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "scene.h"

#include <unordered_map>
using namespace std;

class SceneManager
{
  public:
		static SceneManager* instance;

    SceneManager();
    virtual ~SceneManager();

    template <typename S>
    static void Add(string name = "")
    {
      static_assert(is_base_of<Scene, S>::value, "[SceneManager::newTask] S must derive from Scene");

      Scene* scene = new S(name);

      unsigned int _id = 0;
      string id = name + to_string(_id);

      while(isInList(id))
      {
        _id++;
        id = name + to_string(_id);
      }

      SceneManager::instance->sceneMap[id] = scene;
    }

    static bool isInList(string key);
    static Scene* Get(string key);

  private:
    unordered_map<string, Scene*> sceneMap;
};

#endif //SCENEMANAGER_H
