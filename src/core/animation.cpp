#include "animation.h"

Animation::Animation(const char *p, const char *b, int f, float s, bool l)
{
  save(p, b, f, s, l);
}

Animation::Animation(const char *b, int f, float s, bool l)
{
  save("", b, f, s, l);
}

void Animation::save(const char *p, const char *b, int f, float s, bool l)
{
  looped = l;
  package = string(p);
  basename = string(b);
  frameAmount = f;
  speed = s;
}
