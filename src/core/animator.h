#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "animation.h"

#include <SDL2/SDL.h>

#include <string>
#include <map>
using namespace std;

class Animator
{
  public:
    Animator(SDL_Surface **g = nullptr);
    virtual ~Animator();

    void setGraph(SDL_Surface **g);

    void Add(string aID, const char *b, int f, float s = 1.0, bool l = true);
    void Add(string aID, const char *p, const char *b, int f, float s = 1.0, bool l = true);

    bool isInList(string key);
    Animation* Get(string key);

    void tryToExecute(string state);
    void Draw();

    void setSpeed(float speed);
    float getSpeed();

  private:
    SDL_Surface **graph;
    map<string, Animation*> animationList;
    string selectedAnimation;
    Animation* actualAnimation;

    float actualFrame;
};

#endif //ANIMATOR_H
