#ifndef SOLID_H
#define SOLID_H

#include "sprite.h"
#include "camera.h"
#include "interfaces/alive.h"
#include <RosquilleraReforged/rf_engine.h>

#include <SDL2/SDL.h>
#include <vector>
#include <map>
#include <cstdlib>
using namespace std;

class Solid;

struct BoxCollider
{
  int id;
  SDL_Rect collider;
  bool isTrigger = false;
  Solid* solid;

  BoxCollider(Solid* owner, int _id, SDL_Rect col, bool istrigger = false)
  {
    solid = owner;
    id = _id;
    collider = col;
    isTrigger = istrigger;
  }

  RF_Process* box = nullptr;
  void View(SDL_Rect n)
  {
    if(box == nullptr && n.w > 1 && n.h > 1)
    {
      RF_Engine::Debug(collider.w);
      box = RF_Engine::getTask<RF_Process>(RF_Engine::newTask<RF_Process>());
      box->zLayer = id;
      box->graph = SDL_CreateRGBSurface(0, n.w, n.h, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
      SDL_FillRect(box->graph, NULL, SDL_MapRGB(box->graph->format, 1+rand()%255, 1+rand()%255, 1+rand()%255));
    }

    if(box != nullptr)
    {
      box->transform.position.x = n.x + (n.w >> 1) + Camera::instance->transform.position.x;
      box->transform.position.y = n.y + (n.h >> 1) + Camera::instance->transform.position.y;
    }
  }
};

class Solid : public Sprite, public Alive
{
  public:
    Solid(string name = "solid");
    virtual ~Solid();

    virtual void Start();
    virtual void Awake(){}
    virtual void Update();
    virtual void FixedUpdate(){}
    virtual void Draw();

    void IsColliding(int _id, BoxCollider* collider);
    virtual void OnCollision(int _id, BoxCollider* collider){}
    virtual void OnTrigger(int _id, BoxCollider* collider){}

    int addCollider(int x_offset, int y_offset, int width, int height, bool istrigger = false);
    void setCollider(int _id, int x_offset, int y_offset, int width, int height, bool istrigger = false);
    void setToTrigger(int _id = 0, bool istrigger = true);

    SDL_Rect normalizeBound(int _id);

    vector<BoxCollider> boxColliderList;
    Vector2<float> force = Vector2<float>(0.0, 0.0);
};

class SolidCollisionCoroutine : public RF_Process
{
  public:
    SolidCollisionCoroutine():RF_Process("SolidCollisionCoroutine"){}
    virtual ~SolidCollisionCoroutine();

    static void Init();
    static void RegisterSolid(Solid* solid);
    static void RemoveSolid(string solid);
    static bool IsRegistered(string solid);

    virtual void Update();

  private:
    map<string, Solid*> solidList;
    static string instanceid;
};

#endif //SOLID_H
