#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <map>
using namespace std;

class InputManager
{
  public:
    static void setAxis(string name, bool* positive, bool* negative);
    static int getAxis(string name);

    static bool False;

  private:
    static map<string, pair<bool*, bool*>> Axis;
};

#endif //INPUTMANAGER_H
