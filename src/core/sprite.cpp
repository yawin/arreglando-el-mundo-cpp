#include "sprite.h"
#include "camera.h"

Sprite::Sprite(string name):RF_Process(name)
{
  animator = new Animator(&graph);
}

Sprite::~Sprite()
{
  delete animator;
}

void Sprite::Draw()
{
  animator->Draw();

  offset.x = -(graph->w >> 1);
  offset.y = -(graph->h);
  zLayer = realPosition.y;

  OnSpriteDraw();
}

void Sprite::LateDraw()
{
  if(Camera::instance != nullptr)
  {
    transform.position = Vector2<float>(realPosition.x + Camera::instance->transform.position.x + offset.x, realPosition.y + Camera::instance->transform.position.y + offset.y);
  }
}
