#ifndef CAMERA_H
#define CAMERA_H

#include <RosquilleraReforged/rf_process.h>

class Camera : public RF_Process
{
  public:
    static Camera* instance;

    Camera();
    virtual ~Camera();

    void Set(string _id);

    template<typename T>
    void RoomSize(T x, T y)
    {
      roomSize.x = (float) x;
      roomSize.y = (float) y;
    }

    Vector2<float> RoomSize();
    inline float& Speed(){return speed;}

    virtual void Draw();

  private:
    Vector2<float> roomSize;
    string target = "";
    float speed = 5.0;
};

#endif //CAMERA_H
