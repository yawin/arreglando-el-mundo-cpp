#ifndef WEAPON_H
#define WEAPON_H

class Weapon;

#include "sprite.h"
#include "actor.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <string>
using namespace std;

class Scope : public Sprite
{
  public:
    Scope(string name = "scope"):Sprite(name){}
    virtual ~Scope(){}

    virtual void Start()
    {
      zLayer = -1;
      OnSpawn();
    }

    virtual void OnSpawn(){}
    virtual void Update();
    virtual void OnSpriteDraw();

    void setScopeAnimation(const char *p, const char *b, int f = 1, float s = 1.0, bool l = true);
    Vector2<float> targeting = Vector2<float>(0.0, 0.0);
    float speed = 1000.0;
};

class Weapon : public Sprite
{
  public:
    Weapon(string name):Sprite(name){}
    virtual ~Weapon();

    virtual void Update();
    virtual void OnSpriteDraw();
    void Equipa(int municion = 1, int cargadores = 0);

    bool reorientable = true;

  protected:
    enum State
    {
      ST_SPAWN = 0,
      ST_STAND = 1,
      ST_SHOOTING = 2,
      ST_RECHARGE = 3,
      ST_NO_AMMO = 4
    };

    int actualState = ST_SPAWN;
    map<int, string> animationRelation;

    virtual void OnSpawn(){}
    virtual void OnStand(){}
    virtual void OnShoot(){}
    virtual void OnRecharge(){}
    virtual void OnNoAmmo(){}

    Actor* assignedTo = nullptr;
    Scope* scope = nullptr;
    template<typename S>
    void setScope()
    {
      if(scope != nullptr)
      {
        RF_Engine::sendSignal(scope->id, S_KILL);
        scope = nullptr;
      }

      scope = RF_Engine::getTask<Scope>(RF_Engine::newTask<S>(id));
      scope->targeting.x = 100;
    }

    void Reorientar(float x, float y);
    int loaders;
    int ammo;
};

#endif //WEAPON_H
