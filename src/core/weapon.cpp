#include "weapon.h"

#include <cmath>
using namespace std;

void Scope::Update()
{
  if(RF_Input::key[_up])
  {
    targeting.y -= speed * RF_Engine::instance->Clock.deltaTime;
  }
  else if(RF_Input::key[_down])
  {
    targeting.y += speed * RF_Engine::instance->Clock.deltaTime;
  }

  if(RF_Input::key[_left])
  {
    targeting.x -= speed * RF_Engine::instance->Clock.deltaTime;
  }
  else if(RF_Input::key[_right])
  {
    targeting.x += speed * RF_Engine::instance->Clock.deltaTime;
  }
}
void Scope::OnSpriteDraw()
{
  offset = RF_Engine::getTask<Sprite>(father)->offset + targeting;
  realPosition = RF_Engine::getTask<Sprite>(father)->realPosition;
}
void Scope::setScopeAnimation(const char *p, const char *b, int f, float s, bool l)
{
  if(!RF_AssetManager::isLoaded(p))
  {
    RF_AssetManager::LoadAssetPackage(p);
  }

  animator->Add("scope", p, b, f, s, l);
  animator->tryToExecute("scope");
}

Weapon::~Weapon()
{
  if(scope != nullptr)
  {
    RF_Engine::sendSignal(scope->id, S_KILL);
    scope = nullptr;
  }
}
void Weapon::Update()
{
  if(assignedTo == nullptr)
  {
    assignedTo = RF_Engine::getTask<Actor>(father);
  }
  else
  {
    realPosition.x = assignedTo->realPosition.x;
    realPosition.y = assignedTo->realPosition.y;
  }

  if(animationRelation[actualState] != "" && animator->isInList(animationRelation[actualState]))
  {
    animator->tryToExecute(animationRelation[actualState]);
  }

  if(scope != nullptr)
  {
    Reorientar(scope->targeting.x, scope->targeting.y);
  }

  switch(actualState)
  {
    case ST_SPAWN:
      OnSpawn();
      actualState = ST_STAND;
      break;
    case ST_STAND:
      OnStand();
      break;
    case ST_SHOOTING:
      OnShoot();
      break;
    case ST_RECHARGE:
      OnRecharge();
      break;
    case ST_NO_AMMO:
      OnNoAmmo();
      break;
  }
}

void Weapon::OnSpriteDraw()
{
  if(assignedTo != nullptr)
  {
    offset.x = assignedTo->offset.x + assignedTo->weaponHandleOffset.x;
    offset.y = assignedTo->offset.y + assignedTo->weaponHandleOffset.y;
  }
}

void Weapon::Reorientar(float x, float y)
{
  if(reorientable)
  {
    float angle = atan(y / x) * 180.0 / 3.141592;
    float tmp = 0;
    if(x > 0)
    {
      if(y > 0) // 270º .. 360º
      {
        tmp = angle;
      }
      else // 0º .. 90º
      {
        tmp = 360 + angle;
      }

      flipType = FLIP_NONE;
    }
    else
    {
      if(y > 0) // 180º .. 270º
      {
        tmp = 180 + angle;
      }
      else // 90º .. 180º
      {
        tmp = 180 + angle;
      }

      flipType = FLIP_V;
    }

    transform.rotation = tmp;
  }
  else if(assignedTo != nullptr)
  {
    flipType = assignedTo->flipType;
  }
}

void Weapon::Equipa(int municion, int cargadores)
{
  ammo = municion;
  loaders = cargadores;
}
