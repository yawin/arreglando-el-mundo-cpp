#include "scenemanager.h"

SceneManager* SceneManager::instance = nullptr;

SceneManager::SceneManager()
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
  }
}

SceneManager::~SceneManager()
{
  instance = nullptr;
}

bool SceneManager::isInList(string key)
{
  return (Get(key) != nullptr);
}

Scene* SceneManager::Get(string key)
{
  return SceneManager::instance->sceneMap[key];
}
