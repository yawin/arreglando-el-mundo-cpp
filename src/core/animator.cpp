#include "animator.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>
#include <assert.h>

Animator::Animator(SDL_Surface **g)
{
  graph = g;
  selectedAnimation = "";
}
Animator::~Animator()
{
  for(auto a : animationList)
  {
    delete a.second;
  }
}

void Animator::setGraph(SDL_Surface **g)
{
  graph = g;
}

void Animator::Add(string aID, const char *b, int f, float s, bool l)
{
  Add(aID, "", b, f, s, l);
}

void Animator::Add(string aID, const char *p, const char *b, int f, float s, bool l)
{
  assert(!isInList(aID));
  animationList[aID] = new Animation(p, b, f, s, l);

  if(selectedAnimation == "")
  {
    tryToExecute(aID);
  }
}

bool Animator::isInList(string key)
{
  return (Get(key) != nullptr);
}

Animation* Animator::Get(string key)
{
  return animationList[key];
}

void Animator::tryToExecute(string state)
{
  assert(isInList(state));
  assert(RF_AssetManager::isLoaded(animationList[state]->Package(), animationList[state]->Basename() + "_0"));

  if(selectedAnimation != state)
  {
    selectedAnimation = state;
    actualAnimation = animationList[selectedAnimation];
    actualFrame = 0.0;
  }
}

void Animator::Draw()
{
  if(actualAnimation != nullptr && selectedAnimation != "")
  {
    actualFrame += (actualAnimation->Speed()*RF_Engine::instance->Clock.deltaTime);

    if(actualFrame < 0.0)
    {
      actualFrame = (actualAnimation->IsLoop()) ? 0.9 + (float)actualAnimation->FrameAmount()-1 : 0.0;
    }
    else if(actualFrame > 0.9 + (float)actualAnimation->FrameAmount()-1)
    {
      actualFrame = (actualAnimation->IsLoop()) ? 0.0 : 0.9 + (float)actualAnimation->FrameAmount()-1;
    }

    (*graph) = RF_AssetManager::Get<RF_Gfx2D>(actualAnimation->Package(), actualAnimation->Basename() + "_" + to_string((int)actualFrame));
  }
}

void Animator::setSpeed(float speed)
{
  assert(actualAnimation != nullptr);
  actualAnimation->Speed() = speed;
}

float Animator::getSpeed()
{
  assert(actualAnimation != nullptr);
  return actualAnimation->Speed();
}
