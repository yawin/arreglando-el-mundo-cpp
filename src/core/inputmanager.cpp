#include "inputmanager.h"

bool InputManager::False = false;
map<string, pair<bool*, bool*>> InputManager::Axis;

void InputManager::setAxis(string name, bool* positive, bool* negative)
{
  Axis[name] = make_pair(positive, negative);
}

int InputManager::getAxis(string name)
{
  int ret = 0;
  if(*Axis[name].first){ ret++; }
  if(*Axis[name].second){ ret--; }
  return ret;
}
