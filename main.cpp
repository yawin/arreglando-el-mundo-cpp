#include <RosquilleraReforged/rf_engine.h>
#include "comandos.h"
#include "mainprocess.h"
#include "core/solid.h"

#include <iostream>
#include <string>
using namespace std;

extern bool windowed;
extern int startingScene;
extern bool viewCollisionBox;

int main(int argc, char *argv[])
{
	bool debug = false;

	if(argc > 1)
	{
		if(strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
		{
			cout << "Modo de uso: ./ArreglandoElMundo [OPCIONES]" << endl;
			cout << "Ejecuta el juego" << endl << endl;
			cout << "Opciones:" << endl;
			cout << "  -h, --help	muestra esta ayuda" << endl;
			cout << "  -d, --debug	activa el modo debug" << endl;
			cout << "  -b, --box	  permite visualizar las CollisionBox" << endl;
			cout << "  -s, --scene	inicia en la escena indicada" << endl;
			cout << "  -w, --window	inicia en modo ventana" << endl << endl;
			cout << "Por ejemplo:" << endl;
			cout << "	./ArreglandoElMundo -d --window --scene 1" << endl << endl;
			cout << "Si tienes algún problema envía un email a <tuzmakel@gmail.com>" << endl << endl;
			exit(0);
		}

		for(int i = 1; i < argc; i++)
		{
			if(strcmp(argv[i], "--debug") == 0 || strcmp(argv[i], "-d") == 0)
			{
				debug = true;
				windowed = true;
			}
			else if(strcmp(argv[i], "--window") == 0 || strcmp(argv[i], "-w") == 0)
			{
				windowed = true;
			}
			else if(strcmp(argv[i], "--box") == 0 || strcmp(argv[i], "-b") == 0)
			{
				viewCollisionBox = true;
			}
			else if(strcmp(argv[i], "--scene") == 0 || strcmp(argv[i], "-s") == 0)
			{
				startingScene = atoi(argv[++i]);
			}
		}
	}

	if(debug){generaComandos();}
	RF_Engine::Start<MainProcess>(debug);
	exit(0);
}
