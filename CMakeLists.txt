cmake_minimum_required(VERSION 2.8.0)
set(CMAKE_CXX_FLAGS "-std=c++17")
project("ArreglandoElMundo")

file(GLOB demo_SRC
    "main.cpp"
    "./src/*.cpp"
    "./src/core/*.cpp"
    "./src/base/*.cpp"
    "./src/game/*.cpp"
)

add_executable("ArreglandoElMundo" ${demo_SRC})
find_library(SDL2_LIBRARY SDL2)
find_library(ROSQUI_LIB Rosquillera)
target_include_directories(ArreglandoElMundo PUBLIC ./src/)
target_link_libraries(ArreglandoElMundo PUBLIC ${ROSQUI_LIB})
target_link_libraries(
  ArreglandoElMundo
  PUBLIC
  ${SDL2_LIBRARY}
  SDL2main
  SDL2_image
  SDL2_mixer
  SDL2_ttf
)
